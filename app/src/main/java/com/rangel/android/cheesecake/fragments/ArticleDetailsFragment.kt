package com.rangel.android.cheesecake.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.rangel.android.cheesecake.R
import com.rangel.android.cheesecake.models.Article
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.article_details_fragment.*

class ArticleDetailsFragment : Fragment() {

    companion object {
        const val EXTRA_ARTICLE = "EXTRA_ARTICLE"

        fun newInstance(article: Article): ArticleDetailsFragment {
            val args = Bundle()
            args.putSerializable(EXTRA_ARTICLE, article)

            val fragment = ArticleDetailsFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater?,
                              container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater?.inflate(R.layout.article_details_fragment,
                container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val article = arguments.get(EXTRA_ARTICLE) as Article

        title_text_view.text = article.title
        content_text_view.text = article.content
        author_text_view.text = article.authors
        label_text_view.text = article.tags.first().label
        source_and_date_text_view.text = "%s %s, %s".format(
                getString(R.string.by), article.website, article.date)
        Picasso.with(context).load(article.imageUrl).placeholder(R.mipmap.ic_launcher).into(image_view)
    }
}