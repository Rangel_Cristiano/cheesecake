package com.rangel.android.cheesecake.services

import com.rangel.android.cheesecake.models.Article
import retrofit2.Call
import retrofit2.http.GET

interface ArticleService {
    @GET("challenge")
    fun articlesList(): Call<ArrayList<Article>>
}
