package com.rangel.android.cheesecake.activities

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.CardView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*

import com.rangel.android.cheesecake.R
import com.rangel.android.cheesecake.models.Article
import com.squareup.picasso.Picasso

class ArticleAdapter(val context: Context,
                     var dataSource: List<Article>)
    : BaseAdapter() {

    private val mInflater: LayoutInflater
            = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    private var mCallBack: OnArticleViewCallback? = null

    override fun getCount(): Int {
        return dataSource.size
    }

    override fun getItem(position: Int): Any {
        return dataSource[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view: View
        val holder: ViewHolder

        if (convertView == null) {
            view = mInflater.inflate(R.layout.article_list_view, parent, false)

            holder = ViewHolder()
            holder.thumbnailImageView = view.findViewById(R.id.image_view) as ImageView
            holder.titleTextView = view.findViewById(R.id.title_text_view) as TextView
            holder.detailTextView = view.findViewById(R.id.detail_text_view) as TextView
            holder.authorsTextView = view.findViewById(R.id.authors_text_view) as TextView
            holder.dateTextView = view.findViewById(R.id.date_text_view) as TextView
            holder.itemView = view.findViewById(R.id.article_view) as LinearLayout
            holder.cardView = view.findViewById(R.id.card_view) as CardView

            view.tag = holder
        } else {
            view = convertView

            holder = convertView.tag as ViewHolder
        }

        holder.itemView.setTag(position)

        holder.itemView.setOnClickListener {
            val article = getItem(position) as Article

            updateView(holder.cardView, true)

            mCallBack!!.onArticleSelected(article)
        }

        val titleTextView = holder.titleTextView
        val detailTextView = holder.detailTextView
        val thumbnailImageView = holder.thumbnailImageView
        val dateTextView = holder.dateTextView
        val authorsTextView = holder.authorsTextView
        val cardView = holder.cardView

        val article = getItem(position) as Article

        titleTextView.text = article.title
        detailTextView.text = article.content
        dateTextView.text = article.date
        authorsTextView.text = article.authors
        Picasso.with(context).load(article.imageUrl).placeholder(R.mipmap.ic_launcher).into(thumbnailImageView)

        updateView(cardView, article.wasRead)

        return view
    }

    private fun updateView(cardView: CardView, wasRead: Boolean) {
        if (wasRead) {
            cardView.setCardBackgroundColor(ContextCompat.getColor(context, R.color.colorSelected))
        } else {
            cardView.setCardBackgroundColor(ContextCompat.getColor(context, android.R.color.holo_blue_dark))
        }
    }

    fun setLoadMoreCallback(callBack: OnArticleViewCallback) {
        mCallBack = callBack
    }

    private class ViewHolder {
        lateinit var titleTextView: TextView
        lateinit var dateTextView: TextView
        lateinit var detailTextView: TextView
        lateinit var authorsTextView: TextView
        lateinit var cardView: CardView
        lateinit var thumbnailImageView: ImageView
        lateinit var itemView: LinearLayout
    }

    interface OnArticleViewCallback {
        fun onArticleSelected(article: Article)
    }
}
