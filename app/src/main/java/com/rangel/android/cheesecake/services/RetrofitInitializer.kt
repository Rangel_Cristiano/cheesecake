package com.rangel.android.cheesecake.services

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitInitializer {

    private val retrofit = Retrofit.Builder()
            .baseUrl("https://cheesecakelabs.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    fun articleService(): ArticleService = retrofit.create(ArticleService::class.java)
}