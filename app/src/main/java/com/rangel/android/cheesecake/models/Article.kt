package com.rangel.android.cheesecake.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Article : Serializable {
    var wasRead: Boolean = false
    @SerializedName("title") val title: String = ""
    @SerializedName("content") val content: String = ""
    @SerializedName("image_url") val imageUrl: String = ""
    @SerializedName("date") val date: String = ""
    @SerializedName("website") val website: String = ""
    @SerializedName("authors") val authors: String = ""
    @SerializedName("tags") val tags: List<Tag> = listOf()
}