package com.rangel.android.cheesecake

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson

class SharedPreferences (context: Context) {
    private val PREFS_NAME = "com.cheesecake.sharedpreferences"
    private val ARTICLES_WAS_READ_SHARED = "articles_was_read_shared"
    private val sharedPreferences: SharedPreferences = context.getSharedPreferences(PREFS_NAME, 0)

    fun saveArticleWasRead(title: String, date: String) {
        val articleSharedPrefs = ArticlePrefs(title, date)

        var articles = ArrayList<ArticlePrefs>()

        val articlesWasRead = readArticlesWasRead()
        if (articlesWasRead != null)
            articles = ArrayList(articlesWasRead)

        articles.add(articleSharedPrefs)

        val serializedArticle = Gson().toJson(articles)
        sharedPreferences.edit().putString(ARTICLES_WAS_READ_SHARED, serializedArticle).apply()
    }

    fun readArticlesWasRead(): List<ArticlePrefs>? {
        val serializedArticles = sharedPreferences.getString(ARTICLES_WAS_READ_SHARED, null)
        return Gson().fromJson(serializedArticles, Array<ArticlePrefs>::class.java)?.toList()
    }

    //model to save only the date and title on shared preferences
    data class ArticlePrefs (
            val title: String = "",
            val date: String = ""
    )
}