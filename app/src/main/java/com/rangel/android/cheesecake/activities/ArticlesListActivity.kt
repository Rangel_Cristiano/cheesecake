package com.rangel.android.cheesecake.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

import com.rangel.android.cheesecake.R
import com.rangel.android.cheesecake.fragments.ArticlesListFragment
import com.rangel.android.cheesecake.models.Article
import com.rangel.android.cheesecake.fragments.ArticleDetailsFragment

class ArticlesListActivity : AppCompatActivity(),
        ArticlesListFragment.OnArticleSelected {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.articles_list_activity)

        if (savedInstanceState == null) {
            val articlesListFragment = ArticlesListFragment()

            supportFragmentManager
                    .beginTransaction()
                    .add(R.id.fragment_list, articlesListFragment)
                    .commit()
        }
    }

    override fun onArticleSelected(article: Article) {
        //decides if implements tablet or smartphone interface
       if (resources.getBoolean(R.bool.isTablet)) {
           val articleDetailsFragment = ArticleDetailsFragment.newInstance(article)

           supportFragmentManager
                   .beginTransaction()
                   .add(R.id.fragment_detail, articleDetailsFragment)
                   .commit()
       } else {
           val intent = ArticleDetailActivity.newIntent(this, article)
           startActivity(intent)
       }
    }
}
