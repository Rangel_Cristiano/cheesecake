package com.rangel.android.cheesecake.fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.PopupMenu
import android.view.*
import com.rangel.android.cheesecake.SharedPreferences

import com.rangel.android.cheesecake.R
import com.rangel.android.cheesecake.activities.ArticleAdapter
import com.rangel.android.cheesecake.models.Article
import com.rangel.android.cheesecake.services.RetrofitInitializer
import kotlinx.android.synthetic.main.articles_list_fragment.*
import org.jetbrains.anko.alert
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ArticlesListFragment : Fragment() {

    companion object {
        const val EXTRA_LIST_ARTICLES = "EXTRA_LIST_ARTICLES"
        lateinit var mSharedPreferences: SharedPreferences
    }

    private lateinit var mArticleListener: OnArticleSelected
    private var mArticles: ArrayList<Article> = ArrayList<Article>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

        mSharedPreferences = SharedPreferences(context)
    }

    override fun onCreateView(inflater: LayoutInflater?,
                              container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater?.inflate(R.layout.articles_list_fragment,
                container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (savedInstanceState == null) {
            loadDataFromService()
        } else {
            mArticles = savedInstanceState.getSerializable(EXTRA_LIST_ARTICLES) as ArrayList<Article>
            if (mArticles.isEmpty()) loadDataFromService()
            else buildList()
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        if (context is OnArticleSelected) {
            mArticleListener = context
        } else {
            throw ClassCastException(context.toString() + " must implement OnArticleSelected.")
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.articles_list_menu, menu)

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_sort_by -> {
            val popMenu = PopupMenu(context, activity.findViewById(R.id.action_sort_by))

            popMenu.menu.add(0, R.id.title, 0, getString(R.string.title))
            popMenu.menu.add(0, R.id.date, 1, getString(R.string.date))
            popMenu.menu.add(0, R.id.label, 2, getString(R.string.label))
            popMenu.menu.add(0, R.id.website, 3, getString(R.string.website))
            popMenu.menu.add(0, R.id.author, 4, getString(R.string.author))

            popMenu.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
                override fun onMenuItemClick(item: MenuItem): Boolean {

                    when (item.itemId) {
                        R.id.title -> {
                            mArticles.sortBy { it.title }
                        }
                        R.id.date -> {
                            mArticles.sortBy { it.date }
                        }
                        R.id.label -> {
                            mArticles.sortBy { it.tags.first().label }
                        }
                        R.id.website -> {
                            mArticles.sortBy { it.website }
                        }
                        R.id.author -> {
                            mArticles.sortBy { it.authors }
                        }
                        else -> {
                            return false
                        }
                    }

                    val adapter = article_list_view?.adapter as ArticleAdapter
                    adapter.notifyDataSetChanged()

                    return true
                }
            })
            popMenu.show()

            true
        }

        else -> {
            super.onOptionsItemSelected(item)
        }
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)

        outState?.putSerializable(EXTRA_LIST_ARTICLES, mArticles)
    }

    private fun loadDataFromService() {
        progress_bar.visibility = View.VISIBLE

        val call = RetrofitInitializer().articleService().articlesList()
        call.enqueue(object: Callback<ArrayList<Article>?> {
            override fun onResponse(call: Call<ArrayList<Article>?>?,
                                    response: Response<ArrayList<Article>?>?) {

                response?.body()?.let {
                    progress_bar?.visibility = View.GONE

                    mArticles = it;
                    updatedArticlesWasRead()
                }
            }

            override fun onFailure(call: Call<ArrayList<Article>?>?,
                                   t: Throwable?) {
                progress_bar?.visibility = View.GONE

                context.alert(t?.message.toString()) {
                    title = getString(R.string.alert)
                    positiveButton(getString(R.string.try_again)) {
                        activity.finish()
                        activity.startActivity(activity.intent)
                    }
                }.show()
            }
        })
    }

    private fun buildList() {
        if (context != null) {
            val adapter = ArticleAdapter(context, mArticles)
            article_list_view?.adapter = adapter

            adapter.setLoadMoreCallback(object : ArticleAdapter.OnArticleViewCallback {
                override fun onArticleSelected(article: Article) {
                    saveArticleWasRead(article)

                    mArticleListener.onArticleSelected(article)
                }
            })
        }
    }

    private fun saveArticleWasRead(article : Article) {
        article.wasRead = true;

        mSharedPreferences.saveArticleWasRead(article.title, article.date)
    }

    //verify if the articles was read comparing the title and date saved on shared preferences
    private fun updatedArticlesWasRead() {
        val articlesWasRead = mSharedPreferences.readArticlesWasRead()

        if (articlesWasRead != null && !articlesWasRead.isEmpty()) {
            for (article in mArticles) {
                article.wasRead = articlesWasRead.any {
                    it.title.equals(article.title) and
                            it.date.equals(article.date)
                }
            }
        }

        buildList()
    }

    interface OnArticleSelected {
        fun onArticleSelected(article: Article)
    }
}