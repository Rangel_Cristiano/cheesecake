package com.rangel.android.cheesecake.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity

import com.rangel.android.cheesecake.R
import com.rangel.android.cheesecake.fragments.ArticleDetailsFragment
import com.rangel.android.cheesecake.models.Article

class ArticleDetailActivity : AppCompatActivity() {

  companion object {
    const val EXTRA_ARTICLE = "EXTRA_ARTICLE"

    fun newIntent(context: Context, article: Article): Intent {
        val intent = Intent(context, ArticleDetailActivity::class.java)
        intent.putExtra(EXTRA_ARTICLE, article)

        return intent
    }
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.article_details_activity)

    if (savedInstanceState == null) {
        val article = intent.extras.get(EXTRA_ARTICLE) as Article

        val articleDetailsFragment = ArticleDetailsFragment.newInstance(article)

        supportFragmentManager
              .beginTransaction()
              .add(R.id.fragment_content, articleDetailsFragment)
              .commit()
    }
  }
}
