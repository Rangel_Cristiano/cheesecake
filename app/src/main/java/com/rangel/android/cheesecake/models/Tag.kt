package com.rangel.android.cheesecake.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Tag : Serializable {
    @SerializedName("id") val id = ""
    @SerializedName("label") val label = ""
}